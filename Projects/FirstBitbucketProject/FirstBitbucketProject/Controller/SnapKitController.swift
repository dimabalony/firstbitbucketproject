//
//  SnapKitController.swift
//  FirstBitbucketProject
//
//  Created by Дмитрий Болоников on 4/16/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import UIKit
import SnapKit

class SnapKitController: UIViewController {

    let profileImageView = RoundedImageView()
    let firstNameLabel = UILabel()
    let secondNameLabel = UILabel()
    let fullNameStackView = UIStackView()

    override func viewDidLoad() {
        super.viewDidLoad()

        profileImageView.image = UIImage(named: "profileImage")
        self.view.addSubview(profileImageView)
        setupImageViewConstrains()

        firstNameLabel.text = "Дмитрий"
        setupLabel(label: firstNameLabel)

        secondNameLabel.text = "Болоников"
        setupLabel(label: secondNameLabel)

        setupFullNameStackView()
        self.view.addSubview(fullNameStackView)
        setupFullNameStackViewConstrains()
    }

    func setupLabel(label: UILabel) {
        label.textAlignment = .center
        label.font = UIFont(name: "Helvetica Neue", size: 22)
    }

    func setupImageViewConstrains() {
        profileImageView.snp.makeConstraints { (make) -> Void in
            make.width.equalTo(profileImageView.snp.height)
            make.centerX.equalTo(self.view.center.x)
            make.left.equalTo(self.view).offset(50)
            if #available(iOS 11.0, *) {
                make.top.equalTo(self.view.safeAreaLayoutGuide).offset(50)
                make.left.equalTo(self.view.safeAreaLayoutGuide).offset(50)
            } else {
                make.top.equalTo(self.view).offset(114)
                make.left.equalTo(self.view).offset(50)
            }
        }
    }

    func setupFullNameStackViewConstrains() {
        fullNameStackView.snp.makeConstraints { (make) in
            make.top.equalTo(profileImageView.snp.bottom).offset(50)
            make.centerX.equalTo(self.view.center.x)
        }
    }

    func setupFullNameStackView() {
        fullNameStackView.addArrangedSubview(firstNameLabel)
        fullNameStackView.addArrangedSubview(secondNameLabel)
        fullNameStackView.axis = .vertical
        fullNameStackView.spacing = 10
    }

}
