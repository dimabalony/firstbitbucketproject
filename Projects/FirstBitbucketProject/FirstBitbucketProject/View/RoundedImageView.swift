//
//  RoundedImage.swift
//  FirstBitbucketProject
//
//  Created by Дмитрий Болоников on 4/10/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import UIKit

class RoundedImageView: UIImageView {
    override func layoutSubviews() {
        super.layoutSubviews()
        setupView()
    }

    func setupView() {
        self.contentMode = UIView.ContentMode.scaleAspectFill
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
    }
}
