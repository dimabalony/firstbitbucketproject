//
//  Collection.swift
//  Concentration
//
//  Created by Дмитрий Болоников on 4/25/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import Foundation

extension Collection {
    var oneAndOnly: Element? {
        return count == 1 ? first : nil
    }
}
