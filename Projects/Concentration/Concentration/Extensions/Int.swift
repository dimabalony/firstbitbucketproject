//
//  Int.swift
//  Concentration
//
//  Created by Дмитрий Болоников on 4/25/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import Foundation

extension Int {
    var randomInt: Int {
        if self > 0 {
            return Int.random(in: 0..<self)
        } else if self < 0 {
            return -Int.random(in: 0..<abs(self))
        } else {
            return 0
        }
    }
}
