//
//  Concentretion.swift
//  Concentration
//
//  Created by Дмитрий Болоников on 4/21/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import Foundation

class Concentration {
    
    private(set) var cards = [ConcentrationCard]()
    
    private var indexOfOneAndOnlyFaceUpCard: Int? {
        get {
            return cards.indices.filter { cards[$0].isFaceUp }.oneAndOnly
        }
        set {
            for index in cards.indices {
                cards[index].isFaceUp = (index == newValue)
            }
        }
    }
    private var startTurn = Date()
    private(set) var flipCount = 0
    private(set) var score = 0
    
    init(numberOfPairsOfCards: Int) {
        assert(numberOfPairsOfCards > 0, "Concentration.init(\(numberOfPairsOfCards)): you must have at least one pair of cards")
        for _ in 1...numberOfPairsOfCards {
            let card = ConcentrationCard()
            cards += [card, card]
        }
        cards.shuffle()
    }
    
    func chooseCard(at index: Int) {
        assert(cards.indices.contains(index), "Concentration.chooseCard(at: \(index)): chosen index not in the cards")
        guard !cards[index].isMatched else {
            return
        }
        guard let matchIndex = indexOfOneAndOnlyFaceUpCard, matchIndex != index else {
            indexOfOneAndOnlyFaceUpCard = index
            return
        }
        if cards[matchIndex] == cards[index] {
            cards[matchIndex].isMatched = true
            cards[index].isMatched = true
            updateScore(with: cards[matchIndex], and: cards[index])
        } else {
            updateScore(with: cards[matchIndex], and: cards[index])
            cards[matchIndex].unmatchedFlip = true
            cards[index].unmatchedFlip = true
        }
        cards[index].isFaceUp = true
        flipCount += 1
    }
    
    private func updateScore(with firstCard: ConcentrationCard, and secondCard: ConcentrationCard) {
        if firstCard.isMatched && secondCard.isMatched {
            score += 2 + bonusPoints()
            return
        }
        score += firstCard.unmatchedFlip ? -1 : 0
        score += secondCard.unmatchedFlip ? -1 : 0
    }
    
    private func bonusPoints() -> Int {
        let turnTime = -startTurn.timeIntervalSince(Date())
        startTurn = Date()
        if turnTime < 5 {
            return 2
        } else if turnTime > 10 {
            return -1
        } else {
            return 0
        }
    }
}
