//
//  Card.swift
//  Concentration
//
//  Created by Дмитрий Болоников on 4/21/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import Foundation

struct ConcentrationCard: Hashable {
    var isFaceUp = false
    var isMatched = false
    var unmatchedFlip = false
    private var identifier: Int
    
    init() {
        self.identifier = ConcentrationCardFactory.instance.getNextIdentifier()
    }
    
    static func == (lhs: ConcentrationCard, rhs: ConcentrationCard) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
}

class ConcentrationCardFactory {
    static let instance = ConcentrationCardFactory()
    private var identifier = 0
    
    func getNextIdentifier() -> Int {
        identifier += 1
        return identifier
    }
}
