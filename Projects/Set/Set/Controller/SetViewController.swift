//
//  SetViewController.swift
//  Set
//
//  Created by Дмитрий Болоников on 4/26/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import UIKit

class SetViewController: UIViewController, GridViewDelegate {
    private(set) var game = Set(numberOfProperties: 4) {
        didSet {
            updateViewFromModel()
            isGameOver()
        }
    }
    var numberOfInitialQuantity: Int = 12
    var againstIPhone = false
    private(set) var timerCount = 0
    private(set) var timer: Timer?
    private(set) var initialTimerValue = 0
    private var isLocked = false
    @IBOutlet weak var setsNumber: UILabel!
    @IBOutlet var gridView: GridView! {
        didSet {
            let rotate = UIRotationGestureRecognizer(target: self, action: #selector(shuffleCards(_:)))
            gridView.addGestureRecognizer(rotate)
        }
    }
    @IBOutlet weak var yourScoreLabel: UILabel!
    @IBOutlet weak var iPhonesScoreLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var findASetButton: UIButton!
    @IBOutlet weak var playgroundView: PlaygroundView! {
        didSet {
            let tap = UITapGestureRecognizer(target: self, action: #selector(touchDealMoreCards))
            playgroundView.deckView.addGestureRecognizer(tap)
            playgroundView.discardPileView.isEnabled = false
        }
    }
    
    override func viewDidLoad() {
        gridView.gridViewDelegate = self
        playgroundView.gridView.cardButtonAnimationDelegate = playgroundView
        if againstIPhone {
            initialTimerValue = 40
        }
        resetTimer()
        NotificationCenter.default.addObserver(self, selector: #selector(resetTimer), name: .resetTimer, object: nil)
        iPhonesScoreLabel.isHidden = !againstIPhone
        game.dialNext(numberOfCards: numberOfInitialQuantity)
    }
    
    @objc func shuffleCards(_ sender: UIRotationGestureRecognizer) {
        game.shuffleCardsInGame()
    }
    
    @IBAction func closeButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { [unowned self] _ in
            self.updateViewFromModel()
        })
    }
    
    @objc func resetTimer() {
        timer?.invalidate()
        timerCount = initialTimerValue
        againstIPhone ? withIPhoneGameTimerStart() : singleGameTimerStart()
        updateTimerColor()
    }
    
    func updateTimerColor() {
        let isRedColor = (self.timerCount > 40 && !againstIPhone) || (self.timerCount < 10 && againstIPhone)
        self.timeLabel.textColor = isRedColor ? .red : .black
        self.timeLabel.text = "Time: \(self.timerCount)"
    }
    
    private func withIPhoneGameTimerStart() {
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (_) in
            self.updateTimerColor()
            self.timerCount -= 1
            var isWon: Bool?
            if self.timerCount < 0 {
                isWon = self.game.iPhonesPick()
                self.updateViewFromModel()
                self.game.removeIPhonePick()
                self.updateViewFromModel()
            }
            self.updateIPhonesFace(isWon)
        })
    }
    
    private func updateIPhonesFace(_ isWon: Bool?) {
        let emoji: Character = {
            switch timerCount {
            case 0...5:   return "🤩"
            case 6...15:  return "🥴"
            case 35...40: guard let isWon = isWon else { fallthrough }
                          return isWon ? "🤪" : "🤯"
            default:      return "🧐"
            }
        }()
        iPhonesScoreLabel.text = "\(emoji)iPhone: \(self.game.opponentsScore)"
    }
    
    private func singleGameTimerStart() {
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (_) in
            self.updateTimerColor()
            self.timeLabel.text = "Time: \(self.timerCount)"
            self.timerCount += 1
        }
    }
    
    func updateViewFromModel() {
        if game.deckOfCards.isEmpty {
            playgroundView.deckView.isEnabled = false
        }
        findASetButton.isEnabled = game.tryTofindASet() != game.selectedCards && game.tryTofindASet() != nil
        yourScoreLabel.text = "Your: \(game.score)"
        setsNumber.text = "\(game.matchedCards.count / 3) Sets"
        playgroundView.deckView.isSelected = game.tryTofindASet() == nil
        gridView.setNeedsDisplay()
    }
    
    func isGameOver() {
        if game.endOfGame {
            var secondScore: Int?
            if againstIPhone {
                secondScore = game.opponentsScore
            }
            let gameOverScreen = GameOverViewController()
            gameOverScreen.setupScores(yourScore: game.score, isIPhone: true, secondScore: secondScore)
            present(gameOverScreen, animated: false) {
                self.newGame()
            }
        }
    }
    
    @IBAction func touchFindASet() {
        if game.selectedCards.count >= 3, game.setControl == .matched {
            game.replaceSelectedMatchedCards()
        }
        game.findSet()
    }
    
    @objc func touchDealMoreCards() {
        guard !game.deckOfCards.isEmpty else { return }
        game.dialNext(numberOfCards: 3)
    }
    
    private func newGame() {
        game = Set(numberOfProperties: 4)
        game.dialNext(numberOfCards: numberOfInitialQuantity)
        resetTimer()
        playgroundView.deckView.isEnabled = true
        playgroundView.discardPileView.isEnabled = false
    }
    
    @IBAction func touchNewGame() {
        newGame()
    }
    
    func gridView(didSelect button: CardButton, at index: Int) {
        guard !isLocked else { return }
        game.chooseCard(at: index)
    }
    
    func gridView(buttonAt index: Int) -> CardButton {
        guard let cardButton = gridView.getButton(for: index) else {
            return CardButton()
        }
        cardButton.configureButton(cardAppearance: CardAppearance(for: game.cardsInGame[index]))
        let correspondingCard = game.cardsInGame[index]
        cardButton.isSelected = game.selectedCards.contains(correspondingCard)
        if (game.selectedCards.count >= 3) && (game.selectedCards.contains(correspondingCard)) {
            cardButton.isMatched = game.setControl == .matched
        }
        return cardButton
    }
    
    func numberOfButtons() -> Int {
        return game.cardsInGame.count
    }
    func updateCycleTimer(didStart isGoing: Bool) {
        isLocked = isGoing
        guard !isGoing, game.selectedCards.count >= 3 else { return }
        game.chooseCard()
    }
    
    func isDeckEmpty() -> Bool {
        return game.deckOfCards.isEmpty
    }
}
