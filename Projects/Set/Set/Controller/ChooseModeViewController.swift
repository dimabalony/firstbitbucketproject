//
//  ChoseModeViewController.swift
//  Set
//
//  Created by Дмитрий Болоников on 5/7/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class ChooseModeViewController: UIViewController, MCBrowserViewControllerDelegate {
    
    var multiplayerSetVC: MultiplayerSetViewController?
    
    func browserViewControllerDidFinish(_ browserViewController: MCBrowserViewController) {
        dismiss(animated: true, completion: nil)
        guard let multiplayerSetVC = multiplayerSetVC, multiplayerSetVC.mcSession.connectedPeers.count > 0 else {
            return
        }
        do {
            let data = DataEnum.initRequest(.init(isFirstTurnByOpponent: true, deckOfCards: multiplayerSetVC.game.deckOfCards))
            let encodedData = try JSONEncoder().encode(data)
            try multiplayerSetVC.mcSession.send(encodedData, toPeers: multiplayerSetVC.mcSession.connectedPeers, with: .reliable)
            multiplayerSetVC.isOpponentsTurn = false
        } catch {
            fatalError()
        }
        present(multiplayerSetVC, animated: true)
    }
    
    func browserViewControllerWasCancelled(_ browserViewController: MCBrowserViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func touchGameButton(_ sender: UIButton) {
        guard let setViewController = storyboard?.instantiateViewController(withIdentifier: "SetVC")  as? SetViewController else {
            assertionFailure("Incorrect Storyboard ID")
            return
        }
        setViewController.againstIPhone = sender.currentTitle == "with iPhone"
        present(setViewController, animated: true, completion: nil)
    }
    
    @IBAction func touchWithFriendGameButton() {
        createActionSheet { (completion, controller) in
            guard completion else { return }
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func createActionSheet(completionHandler: @escaping (Bool, UIAlertController) -> Void) {
        let actionSheet = UIAlertController(title: "Multiplayer", message: "Do you want to Create or Join a game", preferredStyle: .actionSheet)

        actionSheet.addAction(UIAlertAction(title: "Create Game", style: .default, handler: { (_ :UIAlertAction) in
            guard let multiplayerSetVC = self.getMultiplayerSetVC() else {
                return
            }
            multiplayerSetVC.createGame()
            self.present(multiplayerSetVC, animated: true)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Join Game", style: .default, handler: { (_ :UIAlertAction) in
            guard let multiplayerSetVC = self.getMultiplayerSetVC() else {
                return
            }
            self.multiplayerSetVC = multiplayerSetVC
            let mcBrowser = MCBrowserViewController(serviceType: "game", session: multiplayerSetVC.mcSession)
            mcBrowser.delegate = self
            mcBrowser.maximumNumberOfPeers = 2
            mcBrowser.minimumNumberOfPeers = 2
            self.present(mcBrowser, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        completionHandler(true, actionSheet)
    }
    
    func getMultiplayerSetVC() -> MultiplayerSetViewController? {
        guard let multiplayerSetVC = self.storyboard?.instantiateViewController(withIdentifier: "MultiplayerSetVC")  as? MultiplayerSetViewController else {
            assertionFailure("Incorrect Storyboard ID")
            return nil
        }
        multiplayerSetVC.setupConnectivity()
        return multiplayerSetVC
    }
}
