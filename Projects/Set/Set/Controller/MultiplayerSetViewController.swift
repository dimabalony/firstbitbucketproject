//
//  MultiplayerViewController.swift
//  Set
//
//  Created by Дмитрий Болоников on 6/5/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class MultiplayerSetViewController: UIViewController, MCSessionDelegate {
    
    var peerID: MCPeerID!
    var mcSession: MCSession!
    var mcAdvertiserAssistant: MCAdvertiserAssistant!
    
    private(set) var game = SetMultiplayer(numberOfProperties: 4) {
        didSet {
            updateViewFromModel()
            updateButtonsAndLabels()
            isGameOver()
        }
    }
    var numberOfInitialQuantity: Int {
        return (cardButtons.count + 1) / 2
    }
    var isOpponentsTurn: Bool?
    var initialDeckOfCard: [SetCard]?
    @IBOutlet var cardButtons: [CardButton]!
    @IBOutlet weak var yourScoreLabel: UILabel!
    @IBOutlet weak var opponentsScoreLabel: UILabel!
    @IBOutlet weak var dealMoreCardsButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cardButtons.shuffle()
        game.dialNext(numberOfCards: numberOfInitialQuantity, byOpponent: true)
    }
    
    func initGame(_ request: InitGameRequest) {
        isOpponentsTurn = request.isFirstTurnByOpponent
        DispatchQueue.main.async {
            self.cardButtons.forEach({ $0.isEnabled = false })
            self.game = SetMultiplayer(numberOfProperties: 4)
            self.game.setupDeckOfCards(cards: request.deckOfCards)
            self.game.dialNext(numberOfCards: self.numberOfInitialQuantity, byOpponent: true)
        }
    }
    
    private func newGame() {
        cardButtons.forEach({ $0.isEnabled = false })
        cardButtons.shuffle()
        game = SetMultiplayer(numberOfProperties: 4)
        game.dialNext(numberOfCards: numberOfInitialQuantity, byOpponent: false)
    }
    
    func isGameOver() {
        if game.endOfGame {
            let gameOverScreen = GameOverViewController()
            gameOverScreen.setupScores(yourScore: game.score, isIPhone: true, secondScore: game.opponentsScore)
            present(gameOverScreen, animated: false) {
                self.newGame()
            }
        }
    }
    
    @IBAction func touchDealMoreCards() {
        game.dialNext(numberOfCards: 3, byOpponent: false)
        do {
            let data = DataEnum.dealMoreCardTouched
            let encodedData = try JSONEncoder().encode(data)
            try mcSession.send(encodedData, toPeers: mcSession.connectedPeers, with: .reliable)
        } catch {
            assertionFailure("Something went wrong")
        }
    }
    
    @IBAction func touchNewGame() {
        newGame()
    }
    
    @IBAction func touchButton(_ sender: CardButton) {
        guard let isOpponentsTurn = isOpponentsTurn, !isOpponentsTurn else { return }
        guard let cardNumber = cardButtons.firstIndex(of: sender), game.cardsInGame.indices.contains(cardNumber) else {
            print("chosen card was not in cardButtons")
            return
        }
        let threeCardsChosen = game.chooseCard(at: cardNumber)
        self.isOpponentsTurn = threeCardsChosen
        do {
            let data = DataEnum.indexOfCard(cardNumber)
            let encodedData = try JSONEncoder().encode(data)
            try mcSession.send(encodedData, toPeers: mcSession.connectedPeers, with: .reliable)
        } catch {
            fatalError()
        }
    }
    
    func opponentAction(with indexOfCard: Int) {
        DispatchQueue.main.async {
            let threeCardsChosen = self.game.chooseCardByOpponent(indexOfCard)
            self.isOpponentsTurn = !threeCardsChosen
        }
    }
    
    func dealMoreCardTouchedByOpponent() {
        DispatchQueue.main.async {
            self.game.dialNext(numberOfCards: 3, byOpponent: true)
        }
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        do {
            let decoder = JSONDecoder()
            let decodedData = try decoder.decode(DataEnum.self, from: data)

            switch decodedData {
            case .initRequest(let initGameRequest):
                initGame(initGameRequest)
            case .dealMoreCardTouched: dealMoreCardTouchedByOpponent()
            case .indexOfCard(let index): opponentAction(with: index)
            }
        } catch {
            fatalError()
        }
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
    }
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        switch state {
        case .connected:
            print("Connected: \(peerID.displayName)")
        case .connecting:
            print("Connecting: \(peerID.displayName)")
        case .notConnected:
            print("Not Connected: \(peerID.displayName)")
        @unknown default:
            break
        }
    }
    
    func createGame() {
        mcAdvertiserAssistant = MCAdvertiserAssistant(serviceType: "game", discoveryInfo: nil, session: mcSession)
        mcAdvertiserAssistant.start()
    }
    
    func setupConnectivity() {
        peerID = MCPeerID(displayName: UIDevice.current.name)
        mcSession = MCSession(peer: peerID, securityIdentity: nil, encryptionPreference: .required)
        mcSession.delegate = self
    }
    
    func updateViewFromModel() {
        for index in cardButtons.indices {
            let cardButton = cardButtons[index]
            cardButton.isEnabled = game.cardsInGame.indices.contains(index) && !game.cardsInGame[index].excluded
            guard cardButton.isEnabled else { continue }
            let card = game.cardsInGame[index]
            cardButton.cardAppearance = CardAppearance(for: card)
            cardButton.isSelected = game.selectedCards.contains(card)
            guard cardButton.isSelected, game.selectedCards.count >= 3 else { continue }
            cardButton.isMatched = game.setControl == .matched
        }
    }
    
    private func updateButtonsAndLabels() {
        dealMoreCardsButton.isEnabled = game.deckOfCards.count >= 3
            && (cardButtons.count - 3 >= game.cardsInGame.count
                || (game.selectedCards.count >= 3
                    && game.setControl == .matched))
        yourScoreLabel.text = "Your: \(game.score)"
        opponentsScoreLabel.text = "Opponent: \(game.opponentsScore)"
        dealMoreCardsButton.isSelected = game.tryTofindASet() == nil
    }
    
    deinit {
        mcSession.disconnect()
    }
}
