//
//  GameOverViewController.swift
//  Set
//
//  Created by Дмитрий Болоников on 5/3/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import UIKit

class GameOverViewController: UIViewController {
    @IBOutlet weak var yourScoreLabel: UILabel!
    @IBOutlet weak var secondPlayersScoreLabel: UILabel!
    private(set) var yourScore: Int?
    private(set) var secondScore: Int?
    private(set) var isIPhone: Bool = false
    
    override func viewDidLoad() {
        setupYourScore()
        guard let secondScore = self.secondScore else {
            secondPlayersScoreLabel.text = ""
            return
        }
        secondPlayersScoreLabel.text = "\(isIPhone ? "iPhone's" : "Opponent's") Score: \(secondScore)"
    }
    
    func setupYourScore() {
        guard let yourScore = self.yourScore else { return }
        yourScoreLabel.text = "Your Score: \(yourScore)"
    }
    
    func setupScores(yourScore: Int, isIPhone: Bool, secondScore: Int?) {
        guard self.yourScore == nil, self.secondScore == nil else {
            return
        }
        self.yourScore = yourScore
        self.isIPhone = isIPhone
        self.secondScore = secondScore
    }
    
    @IBAction func newGameButton() {
        dismiss(animated: false, completion: nil)
    }
}
