//
//  CardBehavior.swift
//  Set
//
//  Created by Дмитрий Болоников on 6/19/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import UIKit

class CardBehavior: UIDynamicBehavior {
    lazy var collisionBehavior: UICollisionBehavior = {
        let behavior = UICollisionBehavior()
        behavior.translatesReferenceBoundsIntoBoundary = true
        return behavior
    }()
    lazy var itemBehavior: UIDynamicItemBehavior = {
        let behavior = UIDynamicItemBehavior()
        behavior.allowsRotation = false
        behavior.elasticity = 1
        behavior.resistance = 0
        return behavior
    }()
    override init() {
        super.init()
        addChildBehavior(collisionBehavior)
        addChildBehavior(itemBehavior)
    }
    
    convenience init(in animator: UIDynamicAnimator) {
        self.init()
        animator.addBehavior(self)
    }
    
    private func push(_ item: UIDynamicItem) {
        let push = UIPushBehavior(items: [item], mode: .instantaneous)
        if let referenceBounds = dynamicAnimator?.referenceView?.bounds {
            let center = CGPoint(x: referenceBounds.midX, y: referenceBounds.midY)
            switch (item.center.x, item.center.y) {
            case let (xValue, yValue) where xValue < center.x && yValue < center.y:
                push.angle = 2 * CGFloat.pi * 7 / 8
            case let (xValue, yValue) where xValue > center.x && yValue < center.y:
                push.angle = 2 * CGFloat.pi * 5 / 8
            case let (xValue, yValue) where xValue < center.x && yValue > center.y:
                push.angle = 2 * CGFloat.pi * 3 / 8
            case let (xValue, yValue) where xValue > center.x && yValue > center.y:
                push.angle = 2 * CGFloat.pi * 1 / 8
            default:
                push.angle = CGFloat.random(in: 0...(CGFloat.pi * 2))
            }
        }
        push.magnitude = 10
        push.action = { [unowned push, weak self] in
            self?.removeChildBehavior(push)
        }
        addChildBehavior(push)
    }
    
    private func snap(_ item: UIDynamicItem, snapTo point: CGPoint) {
        let snap = UISnapBehavior(item: item, snapTo: point)
        addChildBehavior(snap)
    }
    
    func addItem(_ item: UIDynamicItem, snapTo point: CGPoint, with frame: CGRect) {
        collisionBehavior.addItem(item)
        itemBehavior.addItem(item)
        push(item)
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { (_) in
            self.removeItem(item)
            self.snap(item, snapTo: point)
            (item as? CardButton)?.frame = frame
        }
    }
    
    private func removeItem(_ item: UIDynamicItem) {
        collisionBehavior.removeItem(item)
        itemBehavior.removeItem(item)
    }
}
