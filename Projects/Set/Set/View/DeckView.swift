//
//  StackOfCardView.swift
//  Set
//
//  Created by Дмитрий Болоников on 6/18/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import UIKit

class StackOfCardView: UIView {
    var isSelected = false {
        didSet {
            layer.borderColor = isSelected ? UIColor.blue.cgColor : UIColor.black.cgColor
        }
    }
    var isEnabled = true { didSet { setNeedsDisplay() } }
    override func draw(_ rect: CGRect) {
        if isEnabled, let cardBackImage = UIImage(named: "cardback", in: Bundle(for: self.classForCoder), compatibleWith: traitCollection) {
            layer.cornerRadius = 8
            layer.borderWidth = 2.0
            layer.borderColor = UIColor.black.cgColor
            cardBackImage.draw(in: bounds)
        } else {
            layer.cornerRadius = 0
            layer.borderWidth = 0
        }
    }
}
