//
//  ColorEnum.swift
//  Set
//
//  Created by Дмитрий Болоников on 4/27/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import UIKit

enum ColorEnum: Int {
    case green = 1
    case blue
    case red

    init?(for state: State) {
        guard let value = ColorEnum(rawValue: state.rawValue) else {
            return nil
        }
        self = value
    }
    
    func getValue() -> UIColor {
        switch self {
        case .green:
            return .green
        case .blue:
            return .blue
        case .red:
            return .red
        }
    }
}
