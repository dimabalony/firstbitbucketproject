//
//  ShapeEnum.swift
//  Set
//
//  Created by Дмитрий Болоников on 4/27/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import Foundation

enum ShapeEnum: Int {
    case squiggle = 1
    case diamond
    case oval
    
    init?(for state: State) {
        guard let value = ShapeEnum(rawValue: state.rawValue) else {
            return nil
        }
        self = value
    }

    func getValue() -> Character {
        switch self {
        case .squiggle: return "▲"
        case .diamond:     return "●"
        case .oval:     return "■"
        }
    }
}
