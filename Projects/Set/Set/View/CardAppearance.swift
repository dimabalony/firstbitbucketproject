//
//  CardAppearance.swift
//  Set
//
//  Created by Дмитрий Болоников on 5/4/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import UIKit

struct CardAppearance: Equatable {
    var shape: ShapeEnum
    var color: ColorEnum
    var numberOfShapes: Int
    var shading: ShadingEnum
    private var cardRect: CGRect = .init()
    
    init(for card: SetCard = SetCard(identifier: [State.one, State.one, State.one, State.one])) {
        shape = ShapeEnum(for: card.identifier[0]) ?? ShapeEnum.squiggle
        color = ColorEnum(for: card.identifier[1]) ?? ColorEnum.green
        numberOfShapes = card.identifier[2].rawValue
        shading = ShadingEnum(for: card.identifier[3]) ?? ShadingEnum.solid
    }
    
    static func == (left: CardAppearance, right: CardAppearance) -> Bool {
        return (left.shape == right.shape) && (left.color == right.color) && (left.numberOfShapes == right.numberOfShapes) && (left.shading == right.shading)
    }
        
    mutating func drawSymbols(rect: CGRect) {
        let path = UIBezierPath()
        self.cardRect = rect
        let origin = CGPoint(x: cardRect.midX - (symbolSize.width / 2), y: spaceBetweenSymbols - (symbolSize.height / 2))
        var symbolRect = CGRect(origin: origin, size: symbolSize)
        for _ in 0..<numberOfShapes {
            let shape = getShape(rect: symbolRect)
            path.append(shape)
            symbolRect = symbolRect.offsetBy(dx: 0, dy: spaceBetweenSymbols)
        }
    }
    
    func getShape(rect: CGRect) -> UIBezierPath {
        color.getValue().setStroke()
        color.getValue().setFill()
        let path: UIBezierPath
        switch shape {
        case .squiggle:
            path = getSquiggle(rect: rect)
        case .diamond:
            path = getDiamond(rect: rect)
        case .oval:
            path = getOval(rect: rect)
        }
        path.stroke()
        switch shading {
        case .open:
            break
        case .solid:
            path.fill()
        case .striped:
            let context = UIGraphicsGetCurrentContext()
            context?.saveGState()
            path.addClip()
            drawStrips(path: path, rect: rect)
            context?.restoreGState()
        }
        return path
    }
    
    func drawStrips(path: UIBezierPath, rect: CGRect) {
        path.lineWidth = SizeRatio.stripWidth
        path.move(to: .zero)
        var xValue = rect.minX
        while xValue < rect.maxX {
            path.move(to: CGPoint(x: xValue, y: rect.minY))
            path.addLine(to: CGPoint(x: xValue, y: rect.maxY))
            xValue += SizeRatio.spaceBetweenStrips
        }
        path.stroke()
    }
    
    private func getSquiggle(rect: CGRect) -> UIBezierPath {
        let path = UIBezierPath()
        let widthHalf = symbolSize.width / 2
        let heightHalf = symbolSize.height / 2
        let widthQuarter = widthHalf / 2
        let heightQuarter = heightHalf / 2
        let midXMidY = CGPoint(x: rect.midX, y: rect.midY)
        path.move(to: CGPoint(x: rect.minX, y: rect.midY))
        
        path.addCurve(to: midXMidY.offsetBy(diffX: 0, diffY: -2 * heightHalf / 3), controlPoint1: midXMidY.offsetBy(diffX: -5 * widthHalf / 6, diffY: -heightHalf), controlPoint2: midXMidY.offsetBy(diffX: -widthQuarter, diffY: -heightHalf))
        path.addCurve(to: midXMidY.offsetBy(diffX: 4 * widthHalf / 5, diffY: -heightHalf), controlPoint1: midXMidY.offsetBy(diffX: widthQuarter, diffY: -heightQuarter), controlPoint2: midXMidY.offsetBy(diffX: widthQuarter, diffY: -heightQuarter))
        path.addCurve(to: midXMidY.offsetBy(diffX: widthHalf, diffY: 0), controlPoint1: midXMidY.offsetBy(diffX: widthHalf, diffY: -heightHalf), controlPoint2: midXMidY.offsetBy(diffX: widthHalf, diffY: -heightQuarter))
        
        path.addCurve(to: midXMidY.offsetBy(diffX: 0, diffY: 2 * heightHalf / 3), controlPoint1: midXMidY.offsetBy(diffX: +5 * widthHalf / 6, diffY: heightHalf), controlPoint2: midXMidY.offsetBy(diffX: widthQuarter, diffY: heightHalf))
        path.addCurve(to: midXMidY.offsetBy(diffX: -4 * widthHalf / 5, diffY: heightHalf), controlPoint1: midXMidY.offsetBy(diffX: -widthQuarter, diffY: heightQuarter), controlPoint2: midXMidY.offsetBy(diffX: -widthQuarter, diffY: heightQuarter))
        path.addCurve(to: midXMidY.offsetBy(diffX: -widthHalf, diffY: 0), controlPoint1: midXMidY.offsetBy(diffX: -widthHalf, diffY: heightHalf), controlPoint2: midXMidY.offsetBy(diffX: -widthHalf, diffY: heightQuarter))
        return path
    }
    
    private func getDiamond(rect: CGRect) -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: rect.minX, y: rect.midY))
        path.addLine(to: CGPoint(x: rect.midX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.midY))
        path.addLine(to: CGPoint(x: rect.midX, y: rect.maxY))
        path.close()
        return path
    }
    
    private func getOval(rect: CGRect) -> UIBezierPath {
        let path = UIBezierPath()
        path.addArc(withCenter: CGPoint(x: rect.minX + ovalRadius, y: rect.midY), radius: ovalRadius, startAngle: SizeRatio.ovalFirstAngle, endAngle: SizeRatio.ovalSecondAngle, clockwise: false)
        path.addArc(withCenter: CGPoint(x: rect.maxX - ovalRadius, y: rect.midY), radius: ovalRadius, startAngle: SizeRatio.ovalSecondAngle, endAngle: SizeRatio.ovalFirstAngle, clockwise: false)
        path.close()
        return path
    }
}

extension CardAppearance {
    private struct SizeRatio {
        static let symbolWidthToCardWidth: CGFloat = 0.5
        static let symbolHeightToCardHeight: CGFloat = 0.2
        static let spaceBetweenStrips: CGFloat = 3
        static let stripWidth: CGFloat = 1
        static let ovalFirstAngle = CGFloat.pi * 3 / 2
        static let ovalSecondAngle = CGFloat.pi / 2
    }
    private var ovalRadius: CGFloat {
        return symbolSize.height / 2
    }
    private var cardWidth: CGFloat {
        return cardRect.width
    }
    private var cardHeight: CGFloat {
        return cardRect.height
    }
    private var symbolSize: CGSize {
        return CGSize(width: cardRect.width * SizeRatio.symbolWidthToCardWidth, height: cardRect.height * SizeRatio.symbolHeightToCardHeight)
    }
    private var spaceBetweenSymbols: CGFloat {
        return cardRect.height / CGFloat(numberOfShapes + 1)
    }
}

extension CGPoint {
    func offsetBy(diffX: CGFloat, diffY: CGFloat) -> CGPoint {
        return CGPoint(x: x+diffX, y: y+diffY)
    }
}
