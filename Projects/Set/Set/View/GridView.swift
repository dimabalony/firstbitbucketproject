//
//  GridView.swift
//  Set
//
//  Created by Дмитрий Болоников on 6/12/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import UIKit

class GridView: UIView, CardButtonDelegate {
    var grid = Grid(layout: .aspectRatio(4/3))
    weak var gridViewDelegate: GridViewDelegate?
    weak var cardButtonAnimationDelegate: CardButtonAnimationDelegate?
    override func layoutSubviews() {
        grid.frame = bounds
    }
    weak var updateCycleTimer: Timer?
    
    func touchButton(_ sender: CardButton) {
        guard let index = subviews.firstIndex(of: sender) else { return }
        gridViewDelegate?.gridView(didSelect: sender, at: index)
    }
    
    override func draw(_ rect: CGRect) {
        guard let numberOfButtons = gridViewDelegate?.numberOfButtons() else { return }
        grid.cellCount = numberOfButtons
        updateTimer()
        var delay = 0.0
        for index in 0..<numberOfButtons {
            guard let buttonToAdd = gridViewDelegate?.gridView(buttonAt: index), let frame = grid[index] else { continue }
            let inset = CGFloat(85 / numberOfButtons)
            if !subviews.contains(buttonToAdd) {
                buttonToAdd.delegate = self
                buttonToAdd.frame = frame.insetBy(dx: inset, dy: inset)
                addSubview(buttonToAdd)
                cardButtonAnimationDelegate?.sendButtonToDeck(buttonToAdd, with: delay)
                delay += 0.2
            } else {
                UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.5, delay: 0, options: [], animations: {
                    buttonToAdd.frame = frame.insetBy(dx: inset, dy: inset)
                }, completion: { [unowned self] (_) in
                    if buttonToAdd.isMatched {
                        guard let button = buttonToAdd.mutableCopy() as? CardButton else { return }
                        button.alpha = 1
                        button.isFaceUp = true
                        self.cardButtonAnimationDelegate?.sendButtonToDiscardPile(button)
                        guard self.gridViewDelegate?.isDeckEmpty() == false else {
                            buttonToAdd.removeFromSuperview()
                            return
                        }
                        self.cardButtonAnimationDelegate?.sendButtonToDeck(buttonToAdd, with: delay)
                        delay += 0.2
                    }
                })
            }
            
        }
        for _ in numberOfButtons..<subviews.count {
            subviews[numberOfButtons].removeFromSuperview()
        }
    }
    
    func updateTimer() {
        gridViewDelegate?.updateCycleTimer(didStart: true)
        updateCycleTimer = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false, block: { [unowned self] (_) in
            self.gridViewDelegate?.updateCycleTimer(didStart: false)
        })
    }

    func getButton(for index: Int) -> CardButton? {
        guard let numberOfButtons = gridViewDelegate?.numberOfButtons(), index < numberOfButtons else { return nil }
        guard index < subviews.count, let existingButton = subviews[index] as? CardButton else { return CardButton(frame: .zero) }
        return existingButton
    }
    
    func sendCardToDiscardPile(_ sender: CardButton) {
        cardButtonAnimationDelegate?.sendButtonToDiscardPile(sender)
    }
}

protocol GridViewDelegate: AnyObject {
    func gridView(didSelect button: CardButton, at index: Int)
    func gridView(buttonAt index: Int) -> CardButton
    func numberOfButtons() -> Int
    func updateCycleTimer(didStart isGoing: Bool)
    func isDeckEmpty() -> Bool
}

protocol CardButtonAnimationDelegate: AnyObject {
    func sendButtonToDeck(_ sender: CardButton, with delay: TimeInterval)
    func sendButtonToDiscardPile(_ sender: CardButton)
}
