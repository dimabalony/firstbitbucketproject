//
//  ShadingEnum.swift
//  Set
//
//  Created by Дмитрий Болоников on 4/28/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import UIKit

enum ShadingEnum: Int {
    case solid = 1
    case striped
    case open
    
    init?(for state: State) {
        guard let value = ShadingEnum(rawValue: state.rawValue) else {
            return nil
        }
        self = value
    }
    
    func getShading(with color: UIColor) -> [NSAttributedString.Key: Any] {
        switch self {
        case .solid:    return [.foregroundColor: color.withAlphaComponent(1)]
        case .striped: return [.foregroundColor: color.withAlphaComponent(0.15)]
        case .open:     return [.strokeWidth: 5.0, .strokeColor: color]
        }
    }
}
