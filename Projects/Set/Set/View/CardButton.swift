//
//  CardButton.swift
//  Set
//
//  Created by Дмитрий Болоников on 4/26/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import UIKit

class CardButton: UIButton {
    var cardAppearance: CardAppearance { didSet { setNeedsDisplay() } }
    weak var delegate: CardButtonDelegate?
    override var isSelected: Bool {
        didSet {
            layer.borderColor = isSelected ? UIColor.blue.cgColor : UIColor.black.cgColor
            if !isSelected {
                isMatched = false
            }
        }
    }
    var isMatched = false {
        didSet {
            if isSelected {
                layer.borderColor = isMatched ? UIColor.green.cgColor : UIColor.red.cgColor
                if isMatched {
                    
                } else {
                    shake()
                }
            }
        }
    }
    var isFaceUp = false {
        didSet {
            UIView.transition(with: self, duration: 0.5, options: [.transitionFlipFromLeft], animations: {
                self.setNeedsDisplay()
            })
        }
    }
    init(frame: CGRect = .zero, cardAppearance: CardAppearance = CardAppearance()) {
        self.cardAppearance = cardAppearance
        super.init(frame: frame)
        setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.cardAppearance = CardAppearance()
        super.init(coder: aDecoder)
        setupButton()
    }

    override func draw(_ rect: CGRect) {
        if isFaceUp {
            cardAppearance.drawSymbols(rect: rect)
        } else {
            if let cardBackImage = UIImage(named: "cardback", in: Bundle(for: self.classForCoder), compatibleWith: traitCollection) {
                cardBackImage.draw(in: bounds)
            }
        }
    }

    func setupButton() {
        addTarget(self, action: #selector(touchButton), for: .touchUpInside)
        isOpaque = true
        backgroundColor = UIColor.white
        layer.cornerRadius = 8
        layer.borderWidth = 2.0
        layer.borderColor = UIColor.black.cgColor
        alpha = 0
    }
    
    override func mutableCopy() -> Any {
        return CardButton(frame: frame, cardAppearance: cardAppearance)
    }
    
    func configureButton(cardAppearance: CardAppearance) {
        self.cardAppearance = cardAppearance
    }
    
    @objc func touchButton() {
        delegate?.touchButton(self)
    }
}

protocol CardButtonDelegate: AnyObject {
    func touchButton(_ sender: CardButton)
    func sendCardToDiscardPile(_ sender: CardButton)
}
