//
//  PlaygroundView.swift
//  Set
//
//  Created by Дмитрий Болоников on 6/18/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import UIKit

class PlaygroundView: UIView, CardButtonAnimationDelegate, UIDynamicAnimatorDelegate {
    lazy var animator: UIDynamicAnimator = {
        let animator = UIDynamicAnimator(referenceView: self)
        animator.delegate = self
        return animator
    }()
    lazy var cardBehavior = CardBehavior(in: animator)
    @IBOutlet weak var deckView: StackOfCardView!
    @IBOutlet weak var discardPileView: StackOfCardView!
    @IBOutlet weak var gridView: GridView!
    lazy var deckViewFrame = deckView.frame
    
    func sendButtonToDiscardPile(_ sender: CardButton) {
        addSubview(sender)
        cardBehavior.addItem(sender, snapTo: discardPileView.center, with: discardPileView.frame)
    }
    
    func dynamicAnimatorDidPause(_ animator: UIDynamicAnimator) {
        for view in subviews {
            guard let cardButton = view as? CardButton else { continue }
            cardButton.isFaceUp = false
            Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { (_) in
                self.discardPileView.isEnabled = true
                cardButton.removeFromSuperview()
            }
        }
    }
    
    func sendButtonToDeck(_ sender: CardButton, with delay: TimeInterval) {
        let realFrame = sender.frame
        sender.isFaceUp = false
        sender.frame = deckViewFrame
        sender.alpha = 1
        UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 1, delay: delay, options: [], animations: {
            sender.frame = realFrame
        }, completion: { (_) in
            sender.isFaceUp = true
        })
    }
}
