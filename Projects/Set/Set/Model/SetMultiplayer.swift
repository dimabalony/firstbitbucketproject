//
//  Set.swift
//  Set
//
//  Created by Дмитрий Болоников on 4/26/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import Foundation

struct SetMultiplayer {
    var setControl: SetControl {
        return SetControl(selectedCards: selectedCards)
    }
    private(set) var deckOfCards = [SetCard]()
    private(set) var selectedCards = [SetCard]()
    private var matchedCards = [SetCard]()
    private(set) var cardsInGame = [SetCard]()
    private(set) var score = 0
    private(set) var opponentsScore = 0
    var endOfGame: Bool {
        return deckOfCards.isEmpty && (tryTofindASet() == nil)
    }
    
    init(numberOfProperties: Int) {
        guard var deckOfCards = CardFactory.instance.getNewFullDeck(numberOfProperties: numberOfProperties) else {
            return
        }
        deckOfCards.shuffle()
        self.deckOfCards = deckOfCards
    }
    
    mutating func findSet() {
        guard let set = tryTofindASet() else {
            print("Need more cardsInGame")
            return
        }
        score -= 5
        selectedCards.removeAll()
        selectedCards += set
    }
    
    func tryTofindASet() -> [SetCard]? {
        for firstCard in cardsInGame {
            for secondCard in cardsInGame {
                for thirdCard in cardsInGame {
                    if firstCard != secondCard, firstCard != thirdCard, SetControl(selectedCards: [firstCard, secondCard, thirdCard]) == .matched {
                        return [firstCard, secondCard, thirdCard]
                    }
                }
            }
        }
        return nil
    }
    
    mutating func setupDeckOfCards(cards: [SetCard]) { //for multiplayer game
        deckOfCards = cards
    }
    
    mutating func chooseCard(at index: Int) -> Bool {
        return chooseCardAt(index: index, byOpponent: false)
    }
    
    mutating func chooseCardByOpponent(_ index: Int) -> Bool {
        return chooseCardAt(index: index, byOpponent: true)
    }
    
    mutating func chooseCardAt(index: Int, byOpponent isOpponent: Bool) -> Bool {
        var threeCardsChosen = false
        let card = cardsInGame[index]
        if let index = selectedCards.firstIndex(of: card), selectedCards.count < 3 {
            selectedCards.remove(at: index)
            return threeCardsChosen
        } else if selectedCards.count >= 3 {
            threeCardsChosen = true
            if setControl == .matched {
                isOpponent ? (opponentsScore += 5) : (score += 5)
                replaceSelectedMatchedCards()
                if matchedCards.contains(card) {
                    return threeCardsChosen
                }
            } else {
                isOpponent ? (opponentsScore -= 1) : (score -= 1)
                selectedCards.removeAll()
            }
        }
        selectedCards.append(card)
        return threeCardsChosen
    }
    
    mutating func replaceSelectedMatchedCards() {
        for selectedCard in selectedCards {
            matchedCards.append(selectedCard)
            guard !deckOfCards.isEmpty else {
                guard let index = cardsInGame.firstIndex(of: selectedCard) else { continue }
                cardsInGame[index].excluded = true
                continue
            }
            guard cardsInGame.replaceElement(selectedCard, withElement: deckOfCards.removeFirst()) else {
                preconditionFailure()
            }
        }
        selectedCards.removeAll()
    }
    
    mutating func dialNext(numberOfCards: Int, byOpponent isOpponnent: Bool) {
        if selectedCards.count >= 3, setControl == .matched {
            replaceSelectedMatchedCards()
            return
        }
        if tryTofindASet() != nil {
            if isOpponnent {
                opponentsScore -= 3
            } else {
                score -= 3
            }
        }
        for _ in 0..<numberOfCards {
            guard !deckOfCards.isEmpty else { return }
            let nextCardToAdd = deckOfCards.removeFirst()
            cardsInGame.append(nextCardToAdd)
        }
    }
}
