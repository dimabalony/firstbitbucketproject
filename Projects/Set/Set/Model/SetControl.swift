//
//  SetControl.swift
//  Set
//
//  Created by Дмитрий Болоников on 5/2/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import Foundation

enum SetControl {
    case matched
    case unmatched
    
    init(selectedCards: [SetCard]) {
        guard selectedCards.filter({ $0.excluded }).count == 0 else {
            self = .unmatched
            return
        }
        self = .matched
        isUnmatched(selectedCards)
    }
    
    private mutating func isUnmatched(_ selectedCards: [SetCard]) {
        var sum = 0
        for stateIndex in selectedCards[0].identifier.indices {
            for card in selectedCards {
                sum += card.identifier[stateIndex].rawValue
            }
            if sum % 3 != 0 {
                self = .unmatched
            }
            sum = 0
        }
    }
}
