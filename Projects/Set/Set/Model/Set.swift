//
//  Set.swift
//  Set
//
//  Created by Дмитрий Болоников on 4/26/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import Foundation

struct Set {
    var setControl: SetControl {
        return SetControl(selectedCards: selectedCards)
    }
    private(set) var deckOfCards = [SetCard]()
    private(set) var selectedCards = [SetCard]()
    private(set) var matchedCards = [SetCard]()
    private(set) var cardsInGame = [SetCard]()
    private(set) var score = 0
    private(set) var opponentsScore = 0
    private var startTurn = Date()
    var endOfGame: Bool {
        return deckOfCards.isEmpty && (tryTofindASet() == nil)
    }
    
    init(numberOfProperties: Int) {
        guard var deckOfCards = CardFactory.instance.getNewFullDeck(numberOfProperties: numberOfProperties) else {
            return
        }
        deckOfCards.shuffle()
        self.deckOfCards = deckOfCards
    }
    
    mutating func findSet() {
        guard let set = tryTofindASet() else {
            print("Need more cardsInGame")
            return
        }
        score -= 5
        selectedCards.removeAll()
        selectedCards += set
    }
    
    func tryTofindASet() -> [SetCard]? {
        for firstCard in cardsInGame {
            for secondCard in cardsInGame {
                for thirdCard in cardsInGame {
                    if firstCard != secondCard, firstCard != thirdCard, SetControl(selectedCards: [firstCard, secondCard, thirdCard]) == .matched {
                        return [firstCard, secondCard, thirdCard]
                    }
                }
            }
        }
        return nil
    }
    
    mutating func setupDeckOfCards(cards: [SetCard]) { //for multiplayer game
        deckOfCards = cards
    }
    
    mutating func iPhonesPick() -> Bool {
        while tryTofindASet() == nil {
            dialNext(numberOfCards: 3)
        }
        let isWin = Bool.random()
        if isWin {
            guard let set = tryTofindASet() else {
                assertionFailure("incorrect behavior")
                return false
            }
            selectedCards = set
        } else {
            failPick()
        }
        NotificationCenter.default.post(name: .resetTimer, object: nil)
        if setControl == .matched {
            opponentsScore += 3
            return true
        } else {
            opponentsScore -= 1
            return false
        }
    }
    
    mutating func removeIPhonePick() {
        if  setControl == .matched {
            replaceSelectedMatchedCards()
        }
        selectedCards.removeAll()
        startTurn = Date()
    }
    
    private mutating func failPick() {
        let one = Int.random(in: 0..<cardsInGame.count)
        var two, three: Int
        repeat {
            two = Int.random(in: 0..<cardsInGame.count)
            three = Int.random(in: 0..<cardsInGame.count)
        } while one == two || one == three || two == three
        selectedCards = [cardsInGame[one], cardsInGame[two], cardsInGame[three]]
    }
    
    mutating func chooseCard(at index: Int? = nil) {
        guard let index = index else {
            if selectedCards.count >= 3 {
                if setControl == .matched {
                    score += 3 + bonusPoints()
                    replaceSelectedMatchedCards()
                    NotificationCenter.default.post(name: .resetTimer, object: nil)
                } else {
                    score -= 1
                    selectedCards.removeAll()
                }
            }
            return
        }
        let card = cardsInGame[index]
        if let index = selectedCards.firstIndex(of: card), selectedCards.count < 3 {
            selectedCards.remove(at: index)
        } else {
            selectedCards.append(card)
        }
//        let card = cardsInGame[index]
//        if let index = selectedCards.firstIndex(of: card), selectedCards.count < 3 {
//            selectedCards.remove(at: index)
//            return
//        } else if selectedCards.count >= 3 {
//            if setControl == .matched {
//                score += 3 + bonusPoints()
//                replaceSelectedMatchedCards()
//                NotificationCenter.default.post(name: .resetTimer, object: nil)
//                if matchedCards.contains(card) {
//                    return
//                }
//            } else {
//                score -= 1
//                selectedCards.removeAll()
//            }
//        }
//        selectedCards.append(card)
    }
    
    mutating func chooseCardByOpponent(_ card: SetCard, _ isSelected: Bool) {
        if isSelected {
            selectedCards += [card]
        } else {
            if let index = selectedCards.firstIndex(of: card) {
                selectedCards.remove(at: index)
            }
        }
    }
    
    private mutating func bonusPoints() -> Int {
        let turnTime = -startTurn.timeIntervalSince(Date())
        startTurn = Date()
        if turnTime < 15 {
            return 2
        } else if turnTime > 40 {
            return -2
        } else {
            return 0
        }
    }
    
    mutating func replaceSelectedMatchedCards() {
        guard setControl == .matched else { return }
        for selectedCard in selectedCards {
            matchedCards.append(selectedCard)
            guard !deckOfCards.isEmpty else {
                guard let index = cardsInGame.firstIndex(of: selectedCard) else { continue }
                cardsInGame.remove(at: index)
                continue
            }
            guard cardsInGame.replaceElement(selectedCard, withElement: deckOfCards.removeFirst()) else {
                preconditionFailure()
            }
        }
        selectedCards.removeAll()
        NotificationCenter.default.post(name: .resetTimer, object: nil)
    }
    
    mutating func dialNext(numberOfCards: Int) {
        if selectedCards.count >= 3, setControl == .matched {
            replaceSelectedMatchedCards()
            return
        }
        if tryTofindASet() != nil {
            score -= 3
        }
        for _ in 0..<numberOfCards {
            guard !deckOfCards.isEmpty else { return }
            let nextCardToAdd = deckOfCards.removeFirst()
            cardsInGame.append(nextCardToAdd)
        }
    }
    
    mutating func shuffleCardsInGame() {
        cardsInGame.shuffle()
    }
}
