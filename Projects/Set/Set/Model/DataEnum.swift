//
//  DataEnum.swift
//  Set
//
//  Created by Дмитрий Болоников on 6/20/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import Foundation

enum DataEnum {
    case dealMoreCardTouched
    case indexOfCard(Int)
    case initRequest(InitGameRequest)
    
    var body: Any? {
        switch self {
        case .dealMoreCardTouched: return nil
        case .indexOfCard(let index): return index
        case .initRequest(let initGameRequest): return initGameRequest
        }
    }
}

struct InitGameRequest: Codable {
    var isFirstTurnByOpponent: Bool
    var deckOfCards: [SetCard]
}

extension DataEnum: Codable {
    enum CodingKeys: String, CodingKey {
        case dealMoreCardTouched, indexOfCard, initRequest
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let requestValue = try container.decodeIfPresent(InitGameRequest.self, forKey: .initRequest) {
            self = .initRequest(requestValue)
        } else if let indexOfCardValue = try container.decodeIfPresent(Int.self, forKey: .indexOfCard) {
            self = .indexOfCard(indexOfCardValue)
        } else {
            self = .dealMoreCardTouched
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        switch self {
        case .dealMoreCardTouched: try container.encode(true, forKey: .dealMoreCardTouched)
        case .indexOfCard(let indexOfCard): try container.encode(indexOfCard, forKey: .indexOfCard)
        case .initRequest(let initGameRequest): try container.encode(initGameRequest, forKey: .initRequest)
        }
    }
}
