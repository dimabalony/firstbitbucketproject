//
//  Card.swift
//  Set
//
//  Created by Дмитрий Болоников on 4/26/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import Foundation

struct SetCard: Hashable, Codable {
    private(set) var identifier: [State]
    var excluded = false
    init(identifier: [State]) {
        self.identifier = identifier
    }
}

class CardFactory {
    static let instance = CardFactory()
    private(set) var numberOfProperties: Int?
    private var sizeOfDeck: Int? {
        guard let numberOfProperties = self.numberOfProperties else { return nil }
        return NSDecimalNumber(decimal: pow(3, numberOfProperties)).intValue
    }
    private var states: [State]?
    private(set) var deck: [SetCard]?
    
    func getNewFullDeck(numberOfProperties: Int) -> [SetCard]? {
        self.numberOfProperties = numberOfProperties
        let states = Array.init(repeating: State.one, count: numberOfProperties)
        deck = [SetCard]()
        deck?.append(SetCard(identifier: states))
        self.states = states
        while let nextCard = getNextCard() {
            deck?.append(nextCard)
        }
        return deck
    }
    
    private func getNextCard() -> SetCard? {
        guard var states = self.states,
            let maxSizeOfDeck = self.sizeOfDeck,
            let sizeOfDeck = deck?.count,
            maxSizeOfDeck > sizeOfDeck else { return nil }
        var index = 0
        while states[index].nextState() {
            index += 1
        }
        self.states = states
        return SetCard(identifier: states)
    }
}

enum State: Int, Codable {
    case one = 1
    case two = 2
    case three = 3
    
    mutating func nextState() -> Bool {
        switch self {
        case .one:
            self = .two
            return false
        case .two:
            self = .three
            return false
        case .three:
            self = .one
            return true
        }
    }
}
