//
//  Collection.swift
//  Set
//
//  Created by Дмитрий Болоников on 4/30/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import Foundation

extension Array {
    mutating func replaceElement(_ oldElement: Element, withElement element: Element) -> Bool {
        guard let index = firstIndex(where: { String(describing: $0) == String(describing: oldElement) }) else {
            return false
        }
        self[index] = element
        return true
    }
}
