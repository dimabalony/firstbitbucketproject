//
//  NotificationName.swift
//  Set
//
//  Created by Дмитрий Болоников on 5/9/19.
//  Copyright © 2019 Дмитрий Болоников. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let resetTimer = Notification.Name("resetTimer")
}
